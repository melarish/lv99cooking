from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    title = models.CharField(null=False, max_length=200)
    desc = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    TASK_TYPES = [
        ("Recipe", "Recipe"),
        ("Ingredient", "Ingredient"),
        ("Inspiration", "Inspiration"),  # usually means enjoying someone else's cooking but that helps too!
        ("Nutrition", "Nutrition"),
    ]
    task_type = models.CharField(null=False, default="Recipe", max_length=100, choices=TASK_TYPES)
    TASK_DIFFICULTY = [
        (1, "Easy peasy lemon squeezy (difficulty 1)"),
        (2, "Just don't burn it (difficulty 2)"),
        (3, "Add some flavour (difficulty 3)"),
        (4, "Toss that salad (difficulty 4)"),
        (5, "Drizzle your olive oil from great height (difficulty 5)")
    ]
    diff = models.IntegerField(null=False, default=1, choices=TASK_DIFFICULTY)

    # Will the task appear in Public Tasks
    public = models.BooleanField(default=True)

#     class Meta:
#         abstract = True

    def __unicode__(self):
        # how it shows up in a list on admin interface
        return "Title: " + self.title + ", Type: " + str(self.task_type)


class Character(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    level = models.IntegerField(default=1)
    exp = models.IntegerField(default=0)
    tasks = models.ManyToManyField(Task, through='TaskOwnership', default=None, blank=True)
    difficulty_to_exp = 10  # conversion factor between difficulty and exp

    def __unicode__(self):
        # how it shows up in a list on admin interface
        return "User: " + self.user.username + ", Level: " + str(self.level)

    def update_exp(self, difficulty):
        extra_exp = difficulty * self.difficulty_to_exp
        self.exp += extra_exp
        self.update_level()

    def exp_to_level(self, exp):
        """convert exp to level"""
        return int(exp / 30) + 1

    def update_level(self):
        new_level = self.exp_to_level(self.exp)
        self.level = new_level


class TaskOwnership(models.Model):
    character = models.ForeignKey(Character, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    # Has the task been completed?
    completed = models.BooleanField(default=False)

    def __unicode__(self):
        # how it shows up in a list on admin interface
        return "User: " + self.character.user.username + ", Task: " + self.task.title + ", Completed: " + str(self.completed)
