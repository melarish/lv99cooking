from django.conf.urls import *
# from django.views.generic.simple import *
from views import *
# Enable admin
from django.contrib import admin

admin.autodiscover()

urlpatterns = [
    url(r'^pick$', pick_task, name='pick_task'),
    url(r'^add$', add_task, name='add_task'),
    url(r'^complete$', complete_task, name='complete_task'),
    url(r'^task/(?P<pk>[0-9]+)$', task_detail, name='task_detail'),
    url(r'^', tasks, name='tasks'),
]
