from __future__ import unicode_literals
from django.apps import AppConfig
from django.db.models.signals import post_save

from .signals import post_save_user


class TasksConfig(AppConfig):
    name = 'cooking.tasks'

    def ready(self):
        from django.contrib.auth.models import User
        post_save.connect(post_save_user, sender=User)
