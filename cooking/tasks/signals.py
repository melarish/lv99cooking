def post_save_user(sender, **kwargs):
    from models import Character
    if kwargs['created']:
        instance = kwargs['instance']
        character = Character(user=instance, level=1)
        character.save()
        instance.character = character
        instance.save()
