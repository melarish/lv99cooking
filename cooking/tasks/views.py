from django.shortcuts import render, get_object_or_404, redirect
from models import Task, TaskOwnership
from forms import TaskForm
from json import dumps
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import user_passes_test, login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model


def admin_check(user):
    return user.is_staff


@login_required
@csrf_exempt  # until I figure out how to get CSRF working with JS
# Shows list of tasks for logged in user
def tasks(request):
    user = request.user
    tasks_for_user = user.character.tasks.filter(taskownership__completed=False)
    return render(request, "mine.html", {'tasks': tasks_for_user, 'level': user.character.level,
                                         'exp': user.character.exp})


def task_detail(request, pk):
    task = get_object_or_404(Task, pk=pk)
    return render(request, "task_details.html", {'task': task})


@login_required
def add_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        user = request.user
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            TaskOwnership.objects.create(task=task, character=user.character)
        return redirect('/tasks')
    else:
        form = TaskForm()
        return render(request, "add_task.html", {'form': form})


@login_required
@csrf_exempt  # until I figure out how to get CSRF working with JS
def pick_task(request):
    if request.method == "POST":
        task_id = request.POST.get('ID')
        user = request.user
        task_to_assign = Task.objects.filter(id=task_id)[0]
        TaskOwnership.objects.create(task=task_to_assign, character=user.character)
        # form = TaskForm(request.POST)
        # user = request.user
        # if form.is_valid():
        #     task = form.save(commit=False)
        #     task.save()
        #     user.character.tasks.add(task)
        return redirect('/tasks')
    else:
        task_list = Task.objects.all()
        return render(request, "pick_task.html", {'tasks': task_list})


@login_required
@csrf_exempt  # until I figure out how to get CSRF working with JS
def complete_task(request):
    user = request.user
    if request.method == "POST":  # when complete task is clicked
        task_id = request.POST.get('ID')
        task_to_complete = TaskOwnership.objects.filter(task=task_id, character=user.character)[0]
        task_to_complete.completed = True
        task_to_complete.save()
        user.character.update_exp(task_to_complete.task.diff)
        user.character.save()
    return redirect('/tasks')
