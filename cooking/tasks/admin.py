from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Task, Character, TaskOwnership

admin.site.register(Task)
admin.site.register(Character)
admin.site.register(TaskOwnership)


# Define an inline admin descriptor for Character model
# which acts a bit like a singleton
class CharacterInline(admin.StackedInline):
    model = Character
    can_delete = False
    verbose_name_plural = 'character'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (CharacterInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
