# README #

Like a Duolingo for cooking: start from easy tasks, gradually grow your expertise and level up to Masterchef!

### Current features ###

* Add your own tasks/recipes
* Set task type and difficulty
* Complete tasks to get points
* See a list of public tasks/recipes
* Assign public tasks to yourself

### Upcoming features ###

* Get suggestions for what to do next
* Tags for allergens, dietary preferences
* Comments and ratings
* Full courses for different cuisines